﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HoloLC.TaskDataLib.Managers;
using HoloLC.TaskDataLib.Repositories;
using HoloLC.TaskCoreLib.Models;
using NLog;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HoloLC.TaskWebAPI.Controllers
{
    [Route("api/user/{owner}/[controller]")]
    public class TaskBindersController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public TaskBindersController(IDataRepositoryManager repositoryManager)
        {
            TaskBinderRepository = repositoryManager.TaskBinderRepository;
        }
        public ITaskBinderRepository TaskBinderRepository { get; set; }

        [HttpGet]
        public async Task<IEnumerable<TaskBinder>> FindAllAsync(ulong owner = 0UL)
        {
            var binders = await TaskBinderRepository.FindAllByOwnerAsync(owner);
            return binders;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(ulong id, ulong owner = 0UL)
        {
            var fetched = await TaskBinderRepository.GetItemAsync(id, owner);
            if (fetched == null) {
                return NotFound();
            } else {
                return new ObjectResult(fetched);   // ???
            }
        }

        [HttpPost]
        public async Task PostAsync([FromBody] TaskBinder binder, ulong owner = 0UL)
        {
            var suc = await TaskBinderRepository.AddItemAsync(binder);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpPut("{id}")]
        public async Task PutAsync(ulong id, [FromBody] TaskBinder binder)
        {
            var suc = await TaskBinderRepository.ReplaceItemAsync(binder, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public async Task<int> RemoveAllAsync(ulong owner)
        {
            return await TaskBinderRepository.RemoveAllByOwnerAsync(owner);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(ulong id, ulong owner = 0UL)
        {
            var deleted = await TaskBinderRepository.DeleteItemAsync(id, owner);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}