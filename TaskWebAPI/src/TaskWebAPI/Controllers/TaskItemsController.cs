﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HoloLC.TaskDataLib.Managers;
using HoloLC.TaskDataLib.Repositories;
using HoloLC.TaskCoreLib.Models;
using NLog;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HoloLC.TaskWebAPI.Controllers
{
    [Route("api/board/{board}/[controller]")]
    public class TaskItemsController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public TaskItemsController(IDataRepositoryManager repositoryManager)
        {
            TaskItemRepository = repositoryManager.TaskItemRepository;
        }
        public ITaskItemRepository TaskItemRepository { get; set; }

        [HttpGet]
        public async Task<IEnumerable<TaskItem>> FindAllAsync(ulong board = 0UL)
        {
            var items = await TaskItemRepository.FindAllAsync(board);
            return items;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(ulong id, ulong board = 0UL)
        {
            var fetched = await TaskItemRepository.GetItemAsync(id, board);
            if (fetched == null) {
                return NotFound();
            } else {
                return new ObjectResult(fetched);   // ???
            }
        }

        [HttpPost]
        public async Task PostAsync([FromBody] TaskItem item, ulong board = 0UL)
        {
            var suc = await TaskItemRepository.AddItemAsync(item);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpPut("{id}")]
        public async Task PutAsync(ulong id, [FromBody] TaskItem item)
        {
            var suc = await TaskItemRepository.ReplaceItemAsync(item, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public async Task<int> RemoveAllAsync(ulong board)
        {
            return await TaskItemRepository.RemoveAllAsync(board);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(ulong id, ulong board = 0UL)
        {
            var deleted = await TaskItemRepository.DeleteItemAsync(id, board);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}
