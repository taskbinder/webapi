﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HoloLC.TaskDataLib.Managers;
using HoloLC.TaskDataLib.Repositories;
using HoloLC.TaskCoreLib.Models;
using NLog;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HoloLC.TaskWebAPI.Controllers
{
    [Route("api/user/{owner}/[controller]")]
    public class TaskBoardsController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public TaskBoardsController(IDataRepositoryManager repositoryManager)
        {
            TaskBoardRepository = repositoryManager.TaskBoardRepository;
        }
        public ITaskBoardRepository TaskBoardRepository { get; set; }

        [HttpGet]
        public async Task<IEnumerable<TaskBoard>> FindAllAsync(ulong owner = 0UL)
        {
            var boards = await TaskBoardRepository.FindAllByOwnerAsync(owner);
            return boards;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(ulong id, ulong owner = 0UL)
        {
            var fetched = await TaskBoardRepository.GetItemAsync(id, owner);
            if (fetched == null) {
                return NotFound();
            } else {
                return new ObjectResult(fetched);   // ???
            }
        }

        [HttpPost]
        public async Task PostAsync([FromBody] TaskBoard board, ulong owner = 0UL)
        {
            var suc = await TaskBoardRepository.AddItemAsync(board);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpPut("{id}")]
        public async Task PutAsync(ulong id, [FromBody] TaskBoard board)
        {
            var suc = await TaskBoardRepository.ReplaceItemAsync(board, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public async Task<int> RemoveAllAsync(ulong owner)
        {
            return await TaskBoardRepository.RemoveAllByOwnerAsync(owner);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(ulong id, ulong owner = 0UL)
        {
            var deleted = await TaskBoardRepository.DeleteItemAsync(id, owner);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}
