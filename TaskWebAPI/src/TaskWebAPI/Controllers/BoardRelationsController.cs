﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HoloLC.TaskDataLib.Managers;
using HoloLC.TaskDataLib.Repositories;
using HoloLC.TaskCoreLib.Models;
using NLog;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HoloLC.TaskWebAPI.Controllers
{
    // [Route("api/board/{dependentBoardId}/child/{child}")]
    [Route("api/[controller]")]
    public class BoardRelationsController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public BoardRelationsController(IDataRepositoryManager repositoryManager)
        {
            BoardRelationRepository = repositoryManager.BoardRelationRepository;
        }
        public IBoardRelationRepository BoardRelationRepository { get; set; }


        [HttpGet("dependentBoardId/{dependentBoardId}")]
        public async Task<IEnumerable<BoardRelation>> FindLeadingBoardsAsync(ulong dependentBoardId)
        {
            var boardRelations = await BoardRelationRepository.FindLeadingBoardsAsync(dependentBoardId);
            return boardRelations;
        }

        [HttpPost]
        public async Task PostAsync([FromBody] BoardRelation boardRelation)
        {
            var suc = await BoardRelationRepository.AddBoardRelationAsync(boardRelation);

            // tbd:
            if (suc) {

            } else {

            }
        }

        //[HttpPut("{id}")]
        //public async Task<void Put(ulong id, [FromBody] BoardRelation boardRelation)
        //{
        //    var suc = BoardRelationRepository.ReplaceBoardRelation(boardRelation, true);

        //    // tbd:
        //    if (suc) {

        //    } else {

        //    }
        //}

        [HttpPut("board/{dependentBoardId}/child/{leadingBoardId}")]
        public async Task PutAsync(ulong dependentBoardId, ulong leadingBoardId, [FromBody] BoardRelation boardRelation)
        {
            // assert dependentBoardId == boardRelation.ParentId && child = boardRelation.ChildId
            var suc = await BoardRelationRepository.ReplaceBoardRelationAsync(boardRelation, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public async Task<int> RemoveAllAsync(ulong dependentBoardId)
        {
            return await BoardRelationRepository.RemoveByDependentBoardAsync(dependentBoardId);
        }

        //[HttpDelete("{id}")]
        //public void Delete(ulong dependentBoardId, ulong leadingBoardId)
        //{
        //    BoardRelationRepository.DeleteBoardRelation(id);
        //}
        [HttpDelete("board/{dependentBoardId}/child/{leadingBoardId}")]
        public async Task<IActionResult> DeleteAsync(ulong dependentBoardId, ulong leadingBoardId)
        {
            var deleted = await BoardRelationRepository.DeleteBoardRelationAsync(dependentBoardId, leadingBoardId);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}
