﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Core
{
    // Task progress status.
    public enum TaskItemState
    {
        Unknown = 0,
        Ready = 1,         // Not started.
        Started = 2,       // In progress
        Done = 3,
        Archived = 4,      // Deleted
        // ...

    }

    public static class TaskItemStates
    {
        public static string ToName(this TaskItemState itemState)
        {
            try {
                return Enum.GetName(typeof(TaskItemState), itemState);
            } catch (Exception) {
                return null;
            }
        }
        public static TaskItemState ToTaskItemState(this string value)
        {
            try {
                return (TaskItemState)Enum.Parse(typeof(TaskItemState), value);
            } catch (Exception) {
                return TaskItemState.Unknown;
            }
        }
    }

}
