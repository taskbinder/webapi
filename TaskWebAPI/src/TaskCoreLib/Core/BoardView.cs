﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Core
{
    public enum BoardView
    {
        Unknown = 0,
        UserCollage = 1,    // default value.
        TaskQuadrant,       // priority-urgency axes.
        // horizontal vs vertical ????
        PrioriyBoard,
        UrgencyBoard,
        StatusBoard,        // TaskProgressStatus based
        // ....

    }

    public static class BoardViews
    {
        // isDynamic (user collage vs dynamically generated?)
        public static bool IsDynamic(this BoardView boardView)
        {
            switch(boardView) {
                case BoardView.UserCollage:
                    return false;
                default:
                    return true;
            }
        }

        public static string ToName(this BoardView boardView)
        {
            try {
                return Enum.GetName(typeof(BoardView), boardView);
            } catch (Exception) {
                return null;
            }
        }
        public static BoardView ToBoardView(this string value)
        {
            try {
                return (BoardView)Enum.Parse(typeof(BoardView), value);
            } catch (Exception) {
                return BoardView.Unknown;
            }
        }
    }
}
