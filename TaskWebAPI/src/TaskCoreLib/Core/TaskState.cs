﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Core
{
    // Task progress status.
    public enum TaskState
    {
        Unknown = 0,
        Conceived = 1,     // New. Ideas.
        Planned = 2,       // Not started, Tentatively committed.
        Committed = 3,     // Not started, Will definitely work on this task.
        Active = 11,       // InProgress
        Dormant = 12,      // Work in progress, but not activley working on it at the moment.
        Paused = 13,       // Temporarily stopped working on this task...
        // Done = 21,
        Resolved = 22,
        Closed = 23,
        Postponed = 31,   // "later", Delayed?
        Abandoned = 41,  // ???
        // ...
        // Archieved = 101,  // ???
        // ...

    }

    public static class TaskStates
    {
        public static string ToName(this TaskState taskState)
        {
            try {
                return Enum.GetName(typeof(TaskState), taskState);
            } catch (Exception) {
                return null;
            }
        }
        public static TaskState ToTaskState(this string value)
        {
            try {
                return (TaskState)Enum.Parse(typeof(TaskState), value);
            } catch (Exception) {
                return TaskState.Unknown;
            }
        }
    }

}
