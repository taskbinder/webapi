﻿using HoloLC.TaskCoreLib.Common;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Models
{
    // Represents TaskBoard - Child Board relationship.
    public class BoardChild
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // Data copy
        public void CopyDataFrom(BoardChild source)
        {
            ParentId = source.ParentId;
            ChildId = source.ChildId;
            ChildTitle = source.ChildTitle;
            ChildZOrder = source.ChildZOrder;
            Created = source.Created;
            Updated = source.Updated;
            Deleted = source.Deleted;
        }

        // Parent's type should be TaskBoard
        public ulong ParentId { get; set; }
        // Child can be either TaskBoard or TaskBinder.
        public ulong ChildId { get; set; }

        // For convenience...
        public string ChildTitle { get; set; }
        
        // public BoardSize ChildSize { get; set; }  // Resize the child???
        public ElementPosition ChildPosition { get; set; }
        public int ChildZOrder { get; set; }
        // tbd:
        // overrideable access level, etc. ....
        // ... 

        public long Created { get; set; }
        public long Updated { get; set; }
        public long Deleted { get; set; }
    }
}
