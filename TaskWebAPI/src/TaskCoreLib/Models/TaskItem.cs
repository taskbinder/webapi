﻿using HoloLC.TaskCoreLib.Common;
using HoloLC.TaskCoreLib.Core;
using HoloLC.TaskCoreLib.Models.Core;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Models
{
    public class TaskItem : TaskElement, ITask
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // Data copy
        public void CopyDataFrom(TaskItem source)
        {
            base.CopyDataFrom(source);
            Board = source.Board;
            CloneParent = source.CloneParent;
            ItemType = source.ItemType;
            ItemState = source.ItemState;
            Priority = source.Priority;
            Urgency = source.Urgency;
            Position = source.Position;
            ZOrder = ZOrder;
        }

        public ulong Board { get; set; }   // Item belongs to a single parent TaskBoard.
        // public ulong Owner { get; set; }   // owner information inherits from the Board.Owner.

        // tBD:
        // "do it again"???
        // If CloneParent != null, this task itme is a "do it again" for other item.
        // Clone parent and child should belong to the same board.
        public ulong CloneParent { get; set; }
        // ...

        // Just use the corresponding Board values ???
        public TaskItemType ItemType { get; set; }
        public TaskItemState ItemState { get; set; }
        public int Priority { get; set; }
        public int Urgency { get; set; }


        // size ???
        public ElementPosition Position { get; set; }
        public int ZOrder { get; set; }
        // ...

        // tbd:
        // how to store the actual content?
        // ....

    }
}
