﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Models.Core
{
    public interface ITask
    {
        ulong Id { get; set; }
        string Title { get; set; }
        string Description { get; set; }
        int Priority { get; set; }
        int Urgency { get; set; }
        // type,
        // state
        // ...

    }
}
