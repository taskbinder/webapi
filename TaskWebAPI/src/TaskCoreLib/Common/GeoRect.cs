﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Common
{
    // [ComplexType]
    public struct GeoRect
    {
        public static readonly GeoRect OriginPoint = new GeoRect(GeoPoint.Origin, 0f, 0f);

        public GeoRect(GeoPoint center, float width, float height)
            : this(center.X, center.Y, width, height)
        {
        }
        public GeoRect(float centerX, float centerY, float width, float height)
        {
            CenterX = centerX;
            CenterY = centerY;
            Width = width;
            Height = height;
        }

        public float CenterX { get; set; }
        public float CenterY { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }

        public override string ToString()
        {
            return $"({CenterX},{CenterY})--{Width}x{Height}";
        }
    }
}
