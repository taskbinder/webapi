﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Common
{
    // [ComplexType]
    public struct GeoDisk
    {
        public static readonly GeoDisk OriginPoint = new GeoDisk(GeoPoint.Origin, 0f);

        public GeoDisk(GeoPoint center, float radius)
            : this(center.X, center.Y, radius)
        {
        }
        public GeoDisk(float centerX, float centerY, float radius)
        {
            CenterX = centerX;
            CenterY = centerY;
            Radius = radius;
        }

        public float CenterX { get; set; }
        public float CenterY { get; set; }
        public float Radius { get; set; }

        public override string ToString()
        {
            return $"({CenterX},{CenterY})->{Radius}";
        }

        public string ToSerializedString()
        {
            return $"{CenterX}:{CenterY}:{Radius}";
        }

    }

    public static class GeoDisks
    {
        public static GeoDisk ToGeoDisk(this string value)
        {
            var parts = value.Split(new char[] { ':' }, 3);
            var centerX = 0f;
            var centerY = 0f;
            var radius = 0f;
            if (parts.Length > 0) {
                try {
                    centerX = Convert.ToSingle(parts[0]);
                } catch (Exception) { }
                if (parts.Length > 1) {
                    try {
                        centerY = Convert.ToSingle(parts[1]);
                    } catch (Exception) { }
                    if (parts.Length > 2) {
                        try {
                            radius = Convert.ToSingle(parts[2]);
                        } catch (Exception) { }
                    }
                }
            }
            return new GeoDisk(centerX, centerY, radius);
        }

    }
}
