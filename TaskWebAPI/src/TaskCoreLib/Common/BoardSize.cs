﻿using HoloLC.TaskCoreLib.Core;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskCoreLib.Common
{
    // [ComplexType]
    public struct BoardSize
    {
        public static readonly BoardSize Null = new BoardSize(0U, 0U);

        public BoardSize(uint width, uint height)
        {
            Width = width;
            Height = height;
        }

        public uint Width { get; set; }
        public uint Height { get; set; }

        public override string ToString()
        {
            //var sb = new StringBuilder();
            //sb.Append(Width).Append("x");
            //sb.Append(Height);
            //return sb.ToString();

            return $"{Width}x{Height}";
        }
    }

    public static class BoardSizes
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // tbd
        // "parse" the string to W, H ??
        // ....


        public static BoardSize ConvertToSize(SizeType type)
        {
            switch (type) {
                case SizeType.TinyTinySquare:
                    return new BoardSize(120U, 120U);
                case SizeType.TinySquare:
                    return new BoardSize(240U, 240U);
                case SizeType.SmallSquare:
                    return new BoardSize(360U, 360U);
                case SizeType.MediumSquare:
                    return new BoardSize(480U, 480U);
                case SizeType.LargeSquare:
                    return new BoardSize(600U, 600U);
                case SizeType.ExLargeSquare:
                    return new BoardSize(600U, 600U);
                case SizeType.ExExLargeSquare:
                    return new BoardSize(720U, 720U);
                case SizeType.ExExExLargeSquare:
                    return new BoardSize(840U, 840U);
                case SizeType.ExExExExLargeSquare:
                    return new BoardSize(960U, 960U);
                case SizeType.ExExExExExLargeSquare:
                    return new BoardSize(1080U, 1080U);
                default:
                    return BoardSize.Null;
            }
        }
    }
}
