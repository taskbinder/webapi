﻿using HoloLC.TaskCoreLib.Common;
using HoloLC.TaskCoreLib.Core;
using HoloLC.TaskCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskDataLib.Repositories
{
    public interface ITaskBinderRepository
    {
        Task<TaskBinder> GetItemAsync(ulong id, ulong owner = 0UL);
        // IEnumerable<TaskBinder> FindAll();
        Task<IEnumerable<TaskBinder>> FindAllByOwnerAsync(ulong owner);
        // IEnumerable<TaskBinder> FindByAccessLevel(AccessLevel level);
        // IEnumerable<TaskBinder> FindByOwnerAndAccessLevel(ulong owner, AccessLevel level);

        // How/where to store actual content ???
        Task<bool> AddItemAsync(TaskBinder taskBinder);
        // TaskBinder CreateItem(ulong owner, AccessLevel level, BoardSize size, string title = null, string description = null, bool isTerminal = false);

        // "Overwrite" semantics If the item does not exist, create one. ?? Or, ignore and return null ??
        Task<bool> ReplaceItemAsync(TaskBinder taskBinder, bool createIfNotExist = false);
        // TaskBinder UpdateAccessLevel(ulong id, AccessLevel level);
        // TaskBinder UpdateTitle(ulong id, string title);

        Task<TaskBinder> DeleteItemAsync(ulong id, ulong owner = 0UL);
        Task<int> RemoveAllByOwnerAsync(ulong owner);
        // int RemoveByOwnerAndAccessLevel(ulong owner, AccessLevel level);
    }
}
