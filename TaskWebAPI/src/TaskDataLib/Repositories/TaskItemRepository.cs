﻿using HoloLC.TaskCoreLib.Common;
using HoloLC.TaskCoreLib.Core;
using HoloLC.TaskCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskDataLib.Repositories
{
    public interface ITaskItemRepository
    {
        Task<TaskItem> GetItemAsync(ulong id, ulong board = 0UL);
        Task<IEnumerable<TaskItem>> FindAllAsync(ulong board);
        // IEnumerable<TaskItem> FindByType(ulong board, ItemType type);

        // How/where to store actual content ???
        Task<bool> AddItemAsync(TaskItem item);
        // TaskItem CreateItem(ulong board, ItemType type, ElementPosition position, int zOrder = 0);

        // "Overwrite" syntax. If the item does not exist, create one. ?? Or, ignore and return null ??
        Task<bool> ReplaceItemAsync(TaskItem taskItem, bool createIfNotExist = false);
        // TaskItem UpdatePosition(ulong id, ElementPosition position);
        // TaskItem UpdateZOrder(ulong id, int zOrder);

        Task<TaskItem> DeleteItemAsync(ulong id, ulong board = 0UL);
        Task<int> RemoveAllAsync(ulong board);
        // int RemoveByType(ulong board, ItemType type);

    }
}
