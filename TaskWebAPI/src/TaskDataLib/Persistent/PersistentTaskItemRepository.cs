﻿using HoloLC.TaskDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskCoreLib.Models;
using NLog;
using HoloLC.TaskDataLib.DB;

namespace HoloLC.TaskDataLib.Persistent
{
    public class PersistentTaskItemRepository : ITaskItemRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly TaskDbContext dbContext;

        public PersistentTaskItemRepository(TaskDbContext dbContext)
        {
            this.dbContext = dbContext;
        }



        public async Task<IEnumerable<TaskItem>> FindAllAsync(ulong board)
        {
            Logger.Trace($"FindAll() board = {board}.");

            var queryable = dbContext.TaskItems
                .Select(o => o)
                .Where(o => o.Board == board);
            if (queryable != null) {
                return queryable.ToList();
            }
            Logger.Info($"FindAll() returing null. board = {board}.");
            return null;
        }

        public async Task<TaskItem> GetItemAsync(ulong id, ulong board = 0)
        {
            Logger.Trace($"GetItem() id = {id}; board = {board}.");

            var taskItem = dbContext.TaskItems.FirstOrDefault(o => o.Id == id && o.Board == board);
            return taskItem;
        }

        public async Task<bool> AddItemAsync(TaskItem taskItem)
        {
            Logger.Trace($"AddItem() ptaskItem = {taskItem}.");

            dbContext.TaskItems.Add(taskItem);
            var saved = dbContext.SaveChanges();
            if (saved > 0) {
                System.Diagnostics.Debug.WriteLine($"MermoShard saved: id = {taskItem.Id}. board = {taskItem.Board}.");
                Logger.Debug($"MermoShard saved: id = {taskItem.Id}. board = {taskItem.Board}.");
                return true;
            } else {
                System.Diagnostics.Debug.WriteLine($"Failed to save MermoShard: id = {taskItem.Id}. board = {taskItem.Board}.");
                Logger.Warn($"Failed to save MermoShard: id = {taskItem.Id}. board = {taskItem.Board}.");
                return false;
            }
        }

        public async Task<bool> ReplaceItemAsync(TaskItem taskItem, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItem() ptaskItem = {taskItem}; createIfNotExist = {createIfNotExist}.");

            int saved = 0;
            var existingTaskItem = dbContext.TaskItems.FirstOrDefault(o => o.Board == taskItem.Board && o.Id == taskItem.Id);
            if (existingTaskItem != null) {
                existingTaskItem.CopyDataFrom(taskItem);
                dbContext.TaskItems.Update(existingTaskItem);
                saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard replaced: id = {taskItem.Id}. board = {taskItem.Board}.");
                    Logger.Debug($"MermoShard replaced: id = {taskItem.Id}. board = {taskItem.Board}.");
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to replace MermoShard: id = {taskItem.Id}. board = {taskItem.Board}.");
                    Logger.Warn($"Failed to replace MermoShard: id = {taskItem.Id}. board = {taskItem.Board}.");
                }
            } else {
                if (createIfNotExist) {
                    dbContext.TaskItems.Add(taskItem);
                    saved = dbContext.SaveChanges();
                    if (saved > 0) {
                        System.Diagnostics.Debug.WriteLine($"New pickupbox added: id = {taskItem.Id}. board = {taskItem.Board}.");
                        Logger.Debug($"New pickupbox added: id = {taskItem.Id}. board = {taskItem.Board}.");
                    } else {
                        System.Diagnostics.Debug.WriteLine($"Failed to add new MermoShard: id = {taskItem.Id}. board = {taskItem.Board}.");
                        Logger.Warn($"Failed to add new MermoShard: id = {taskItem.Id}. board = {taskItem.Board}.");
                    }
                } else {
                    System.Diagnostics.Debug.WriteLine($"MermoShard not found: id = {taskItem.Id}. board = {taskItem.Board}.");
                    Logger.Warn($"MermoShard not found: id = {taskItem.Id}. board = {taskItem.Board}.");
                }
            }
            return (saved > 0);
        }


        public async Task<int> RemoveAllAsync(ulong board)
        {
            Logger.Trace($"RemoveAll() board = {board}.");

            var queryable = dbContext.TaskItems
                .Select(o => o)
                .Where(o => o.Board == board);
            if (queryable != null) {
                var list = queryable.ToList();
                var count = list.Count;
                dbContext.TaskItems.RemoveRange(list);
                var saved = dbContext.SaveChanges();
                System.Diagnostics.Debug.WriteLine($"MermoShard removed: target count = {count}; removed = {saved}. board = {board}.");
                Logger.Debug($"MermoShard removed: target count = {count}; removed = {saved}. board = {board}.");
                return saved;
            }
            return 0;
        }

        public async Task<TaskItem> DeleteItemAsync(ulong id, ulong board = 0)
        {
            Logger.Trace($"DeleteById() board = {board}; id = {id}.");

            var taskItem = dbContext.TaskItems.FirstOrDefault(o => o.Board == board && o.Id == id);
            if (taskItem != null) {
                dbContext.TaskItems.Remove(taskItem);
                var saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard deleted: id = {taskItem.Id}. board = {taskItem.Board}.");
                    Logger.Debug($"MermoShard deleted: id = {taskItem.Id}. board = {taskItem.Board}.");
                    return taskItem;
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to deleted pickupbox: id = {taskItem.Id}. board = {taskItem.Board}.");
                    Logger.Warn($"Failed to deleted pickupbox: id = {taskItem.Id}. board = {taskItem.Board}.");
                    return null;
                }
            }
            return null;
        }


    }
}
