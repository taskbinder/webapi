﻿using HoloLC.TaskDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskCoreLib.Models;
using NLog;
using HoloLC.TaskDataLib.DB;

namespace HoloLC.TaskDataLib.Persistent
{
    public class PersistentTaskBinderRepository : ITaskBinderRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly TaskDbContext dbContext;

        public PersistentTaskBinderRepository(TaskDbContext dbContext)
        {
            this.dbContext = dbContext;
        }


        public async Task<IEnumerable<TaskBinder>> FindAllByOwnerAsync(ulong owner)
        {
            Logger.Trace($"FindAll() owner = {owner}.");

            var queryable = dbContext.TaskBinders
                .Select(o => o)
                .Where(o => o.Owner == owner);
            if (queryable != null) {
                return queryable.ToList();
            }
            Logger.Info($"FindAll() returing null. owner = {owner}.");
            return null;
        }

        public async Task<TaskBinder> GetItemAsync(ulong id, ulong owner = 0)
        {
            Logger.Trace($"GetItem() id = {id}; owner = {owner}.");

            var taskBinder = dbContext.TaskBinders.FirstOrDefault(o => o.Id == id && o.Owner == owner);
            return taskBinder;
        }

        public async Task<bool> AddItemAsync(TaskBinder taskBinder)
        {
            Logger.Trace($"AddItem() ptaskBinder = {taskBinder}.");

            dbContext.TaskBinders.Add(taskBinder);
            var saved = dbContext.SaveChanges();
            if (saved > 0) {
                System.Diagnostics.Debug.WriteLine($"MermoShard saved: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                Logger.Debug($"MermoShard saved: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                return true;
            } else {
                System.Diagnostics.Debug.WriteLine($"Failed to save MermoShard: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                Logger.Warn($"Failed to save MermoShard: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                return false;
            }
        }

        public async Task<bool> ReplaceItemAsync(TaskBinder taskBinder, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItem() ptaskBinder = {taskBinder}; createIfNotExist = {createIfNotExist}.");

            int saved = 0;
            var existingTaskBinder = dbContext.TaskBinders.FirstOrDefault(o => o.Owner == taskBinder.Owner && o.Id == taskBinder.Id);
            if (existingTaskBinder != null) {
                existingTaskBinder.CopyDataFrom(taskBinder);
                dbContext.TaskBinders.Update(existingTaskBinder);
                saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard replaced: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                    Logger.Debug($"MermoShard replaced: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to replace MermoShard: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                    Logger.Warn($"Failed to replace MermoShard: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                }
            } else {
                if (createIfNotExist) {
                    dbContext.TaskBinders.Add(taskBinder);
                    saved = dbContext.SaveChanges();
                    if (saved > 0) {
                        System.Diagnostics.Debug.WriteLine($"New pickupbox added: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                        Logger.Debug($"New pickupbox added: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                    } else {
                        System.Diagnostics.Debug.WriteLine($"Failed to add new MermoShard: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                        Logger.Warn($"Failed to add new MermoShard: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                    }
                } else {
                    System.Diagnostics.Debug.WriteLine($"MermoShard not found: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                    Logger.Warn($"MermoShard not found: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                }
            }
            return (saved > 0);
        }


        public async Task<int> RemoveAllByOwnerAsync(ulong owner)
        {
            Logger.Trace($"RemoveAll() owner = {owner}.");

            var queryable = dbContext.TaskBinders
                .Select(o => o)
                .Where(o => o.Owner == owner);
            if (queryable != null) {
                var list = queryable.ToList();
                var count = list.Count;
                dbContext.TaskBinders.RemoveRange(list);
                var saved = dbContext.SaveChanges();
                System.Diagnostics.Debug.WriteLine($"MermoShard removed: target count = {count}; removed = {saved}. owner = {owner}.");
                Logger.Debug($"MermoShard removed: target count = {count}; removed = {saved}. owner = {owner}.");
                return saved;
            }
            return 0;
        }

        public async Task<TaskBinder> DeleteItemAsync(ulong id, ulong owner = 0)
        {
            Logger.Trace($"DeleteById() owner = {owner}; id = {id}.");

            var taskBinder = dbContext.TaskBinders.FirstOrDefault(o => o.Owner == owner && o.Id == id);
            if (taskBinder != null) {
                dbContext.TaskBinders.Remove(taskBinder);
                var saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard deleted: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                    Logger.Debug($"MermoShard deleted: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                    return taskBinder;
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to deleted pickupbox: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                    Logger.Warn($"Failed to deleted pickupbox: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                    return null;
                }
            }
            return null;
        }

    }
}
