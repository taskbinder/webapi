﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskDataLib.Repositories;
using HoloLC.TaskDataLib.Memory;
using NLog;

namespace HoloLC.TaskDataLib.Managers.Impl
{
    public sealed class MemoryRepositoryManager : IDataRepositoryManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public MemoryRepositoryManager()
        {
        }

        private IBinderChildRepository binderChildRepository = new MemoryBinderChildRepository();
        private IBoardChildRepository boardChildRepository = new MemoryBoardChildRepository();
        private ITaskBinderRepository taskBinderRepository = new MemoryTaskBinderRepository();
        private ITaskBoardRepository taskBoardRepository = new MemoryTaskBoardRepository();
        private ITaskItemRepository taskItemRepository = new MemoryTaskItemRepository();
        private IBoardRelationRepository boardRelationRepository = new MemoryBoardRelationRepository();


        public IBinderChildRepository BinderChildRepository
        {
            get
            {
                return binderChildRepository;
            }
        }

        public IBoardChildRepository BoardChildRepository
        {
            get
            {
                return boardChildRepository;
            }
        }

        public ITaskBinderRepository TaskBinderRepository
        {
            get
            {
                return taskBinderRepository;
            }
        }

        public ITaskBoardRepository TaskBoardRepository
        {
            get
            {
                return taskBoardRepository;
            }
        }

        public ITaskItemRepository TaskItemRepository
        {
            get
            {
                return taskItemRepository;
            }
        }

        public IBoardRelationRepository BoardRelationRepository
        {
            get
            {
                return boardRelationRepository;
            }
        }

    }
}
