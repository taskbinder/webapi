﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskDataLib.Repositories;
using HoloLC.TaskDataLib.Persistent;
using NLog;
using HoloLC.TaskDataLib.DB;

namespace HoloLC.TaskDataLib.Managers.Impl
{
    public sealed class PersistentRepositoryManager : IDataRepositoryManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();


        private readonly IBinderChildRepository binderChildRepository;
        private readonly IBoardChildRepository boardChildRepository;
        private readonly ITaskBinderRepository taskBinderRepository;
        private readonly ITaskBoardRepository taskBoardRepository;
        private readonly ITaskItemRepository taskItemRepository;
        private readonly IBoardRelationRepository boardRelationRepository;

        public PersistentRepositoryManager(TaskDbContext dbContext)
        {
            binderChildRepository = new PersistentBinderChildRepository(dbContext);
            boardChildRepository = new PersistentBoardChildRepository(dbContext);
            taskBinderRepository = new PersistentTaskBinderRepository(dbContext);
            taskBoardRepository = new PersistentTaskBoardRepository(dbContext);
            taskItemRepository = new PersistentTaskItemRepository(dbContext);
            boardRelationRepository = new PersistentBoardRelationRepository(dbContext);
       }


        public IBinderChildRepository BinderChildRepository
        {
            get
            {
                return binderChildRepository;
            }
        }

        public IBoardChildRepository BoardChildRepository
        {
            get
            {
                return boardChildRepository;
            }
        }

        public ITaskBinderRepository TaskBinderRepository
        {
            get
            {
                return taskBinderRepository;
            }
        }

        public ITaskBoardRepository TaskBoardRepository
        {
            get
            {
                return taskBoardRepository;
            }
        }

        public ITaskItemRepository TaskItemRepository
        {
            get
            {
                return taskItemRepository;
            }
        }

        public IBoardRelationRepository BoardRelationRepository
        {
            get
            {
                return boardRelationRepository;
            }
        }

    }
}
