﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskDataLib.Repositories;
using HoloLC.TaskDataLib.Dynamo;
using NLog;
using HoloLC.TaskDataLib.DB;
using HoloLC.TaskDataLib.Dynamo.Tables;
using AWSCore.DynamoBase.Tables.Core;
using AWSCore.DynamoBase.Repositories;

namespace HoloLC.TaskDataLib.Managers.Impl
{
    public sealed class DynamoRepositoryManager : IInstantRepositoryManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();


        private readonly IBinderChildRepository binderChildRepository;
        private readonly IBoardChildRepository boardChildRepository;
        private readonly ITaskBinderRepository taskBinderRepository;
        private readonly ITaskBoardRepository taskBoardRepository;
        private readonly ITaskItemRepository taskItemRepository;
        private readonly IBoardRelationRepository boardRelationRepository;

        public DynamoRepositoryManager(ITableCollection tableCollection)
        {
            binderChildRepository = new DynamoBinderChildRepository(
                tableCollection.GetTable(DynamoBinderChildTable.TABLE_KEY)
                ?? new DynamoBinderChildTable(tableCollection.ClientContext));
            boardChildRepository = new DynamoBoardChildRepository(
                tableCollection.GetTable(DynamoBoardChildTable.TABLE_KEY)
                ?? new DynamoBoardChildTable(tableCollection.ClientContext));
            taskBinderRepository = new DynamoTaskBinderRepository(
                tableCollection.GetTable(DynamoTaskBinderTable.TABLE_KEY)
                ?? new DynamoTaskBinderTable(tableCollection.ClientContext));
            taskBoardRepository = new DynamoTaskBoardRepository(
                tableCollection.GetTable(DynamoTaskBoardTable.TABLE_KEY)
                ?? new DynamoTaskBoardTable(tableCollection.ClientContext));
            taskItemRepository = new DynamoTaskItemRepository(
                tableCollection.GetTable(DynamoTaskItemTable.TABLE_KEY)
                ?? new DynamoTaskItemTable(tableCollection.ClientContext));
            boardRelationRepository = new DynamoBoardRelationRepository(
                tableCollection.GetTable(DynamoBoardRelationTable.TABLE_KEY)
                ?? new DynamoBoardRelationTable(tableCollection.ClientContext));
        }


        public IBinderChildRepository BinderChildRepository
        {
            get
            {
                return binderChildRepository;
            }
        }

        public IBoardChildRepository BoardChildRepository
        {
            get
            {
                return boardChildRepository;
            }
        }

        public ITaskBinderRepository TaskBinderRepository
        {
            get
            {
                return taskBinderRepository;
            }
        }

        public ITaskBoardRepository TaskBoardRepository
        {
            get
            {
                return taskBoardRepository;
            }
        }

        public ITaskItemRepository TaskItemRepository
        {
            get
            {
                return taskItemRepository;
            }
        }

        public IBoardRelationRepository BoardRelationRepository
        {
            get
            {
                return boardRelationRepository;
            }
        }


        public async Task CreateAllAsync(bool recreateIfPresent = false)
        {
            await (BinderChildRepository as IInstantRepository)?.CreateTableAsync(recreateIfPresent);
            await (BoardChildRepository as IInstantRepository)?.CreateTableAsync(recreateIfPresent);
            await (TaskBinderRepository as IInstantRepository)?.CreateTableAsync(recreateIfPresent);
            await (TaskBoardRepository as IInstantRepository)?.CreateTableAsync(recreateIfPresent);
            await (TaskItemRepository as IInstantRepository)?.CreateTableAsync(recreateIfPresent);
            await (BoardRelationRepository as IInstantRepository)?.CreateTableAsync(recreateIfPresent);
        }

        public async Task DeleteAllAsync()
        {
            await (BinderChildRepository as IInstantRepository)?.DeleteTableAsync();
            await (BoardChildRepository as IInstantRepository)?.DeleteTableAsync();
            await (TaskBinderRepository as IInstantRepository)?.DeleteTableAsync();
            await (TaskBoardRepository as IInstantRepository)?.DeleteTableAsync();
            await (TaskItemRepository as IInstantRepository)?.DeleteTableAsync();
            await (BoardRelationRepository as IInstantRepository)?.DeleteTableAsync();
        }

        public async Task PurgeAllAsync(bool createIfAbsent = false)
        {
            await (BinderChildRepository as IInstantRepository)?.PurgeTableAsync(createIfAbsent);
            await (BoardChildRepository as IInstantRepository)?.PurgeTableAsync(createIfAbsent);
            await (TaskBinderRepository as IInstantRepository)?.PurgeTableAsync(createIfAbsent);
            await (TaskBoardRepository as IInstantRepository)?.PurgeTableAsync(createIfAbsent);
            await (TaskItemRepository as IInstantRepository)?.PurgeTableAsync(createIfAbsent);
            await (BoardRelationRepository as IInstantRepository)?.PurgeTableAsync(createIfAbsent);
        }

    }
}
