﻿using HoloLC.TaskCoreLib.Common;
using HoloLC.TaskCoreLib.Models;
using Microsoft.EntityFrameworkCore;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskDataLib.DB
{
    public class TaskDbContext : DbContext
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public TaskDbContext(DbContextOptions<TaskDbContext> options)
            : base(options)
        {
        }

        public DbSet<TaskBinder> TaskBinders { get; set; }
        public DbSet<TaskBoard> TaskBoards { get; set; }
        public DbSet<TaskItem> TaskItems { get; set; }
        public DbSet<BinderChild> BinderChildren { get; set; }
        public DbSet<BoardChild> BoardChildren { get; set; }
        public DbSet<BoardRelation> BoardRelations { get; set; }

        // ????
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Set this in the web api module???
            // optionsBuilder.UseSqlite("connection string");
            // ....
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<TaskBinder>().HasKey(m => m.Id);
            builder.Entity<TaskBoard>().HasKey(m => m.Id);
            builder.Entity<TaskItem>().HasKey(m => m.Id);
            builder.Entity<BinderChild>().HasKey(m => new { m.BinderId, m.BoardId });
            builder.Entity<BoardChild>().HasKey(m => new { m.ParentId, m.ChildId });
            builder.Entity<BoardRelation>().HasKey(m => new { m.LeadingBoardId, m.DependentBoardId });
            builder.Entity<ElementPosition>().HasKey(m => new { m.X, m.Y });

            // Exclude "derived" fields...
            // builder.Entity<TaskBinder>().Ignore(nameof(TaskBinder.Key)).Ignore(nameof(TaskBinder.Size));
            builder.Entity<TaskBinder>().Ignore(nameof(TaskBinder.Size));
            builder.Entity<TaskBoard>().Ignore(nameof(TaskBoard.Size));

            base.OnModelCreating(builder);
        }

    }
}
