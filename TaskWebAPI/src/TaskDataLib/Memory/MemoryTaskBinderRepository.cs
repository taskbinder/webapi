﻿using HoloLC.TaskDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskCoreLib.Common;
using HoloLC.TaskCoreLib.Core;
using HoloLC.TaskCoreLib.Models;
using System.Collections.Concurrent;
using NLog;

namespace HoloLC.TaskDataLib.Memory
{
    public class MemoryTaskBinderRepository : ITaskBinderRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly ConcurrentDictionary<ulong, IDictionary<ulong, TaskBinder>> taskBinderMap = new ConcurrentDictionary<ulong, IDictionary<ulong, TaskBinder>>();


        public async Task<TaskBinder> GetItemAsync(ulong id, ulong owner = 0UL)
        {
            if (owner > 0UL) {
                IDictionary<ulong, TaskBinder> map;
                if (taskBinderMap.TryGetValue(owner, out map)) {
                    if (map.ContainsKey(id)) {
                        var taskBinder = map[owner];
                        return taskBinder;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public async Task<IEnumerable<TaskBinder>> FindAllByOwnerAsync(ulong owner)
        {
            IDictionary<ulong, TaskBinder> map;
            if (taskBinderMap.TryGetValue(owner, out map)) {
                var collection = map.Values;
                return collection;
            } else {
                return null;
            }
        }


        public async Task<bool> AddItemAsync(TaskBinder taskBinder)
        {
            var id = taskBinder.Id;
            var owner = taskBinder.Owner;
            IDictionary<ulong, TaskBinder> map;
            if (taskBinderMap.TryGetValue(owner, out map)) {
                // tbd:
                // This does not gurantee the uniqueness of the id across different owners....
                if (map.ContainsKey(id)) {
                    System.Diagnostics.Debug.WriteLine($"TaskBinder cannot be added because an item with the given id already exists: id = {id}.");
                    return false;
                }
            } else {
                map = new Dictionary<ulong, TaskBinder>();
                taskBinderMap[owner] = map;
            }

            // TBD: Update the Created/Updated field?
            map[id] = taskBinder;
            return true;
        }

        public async Task<bool> ReplaceItemAsync(TaskBinder taskBinder, bool createIfNotExist = false)
        {
            var id = taskBinder.Id;
            var owner = taskBinder.Owner;
            IDictionary<ulong, TaskBinder> map;
            if (taskBinderMap.TryGetValue(owner, out map)) {
            } else {
                if (createIfNotExist) {
                    map = new Dictionary<ulong, TaskBinder>();
                    taskBinderMap[owner] = map;
                } else {
                    return false;
                }
            }

            // TBD: Update the Updated field?
            map[id] = taskBinder;
            return true;
        }


        public async Task<TaskBinder> DeleteItemAsync(ulong id, ulong owner = 0UL)
        {
            if (owner > 0UL) {
                IDictionary<ulong, TaskBinder> map;
                if (taskBinderMap.TryGetValue(owner, out map)) {
                    var taskBinder = map[id];
                    if (map.Remove(id)) {
                        return taskBinder;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public async Task<int> RemoveAllByOwnerAsync(ulong owner)
        {
            IDictionary<ulong, TaskBinder> map;
            if (taskBinderMap.TryRemove(owner, out map)) {
                return map.Count;
            } else {
                return 0;
            }
        }

        //public int RemoveByType(ulong owner, ItemType type)
        //{
        //    throw new NotImplementedException();
        //}

    }
}
