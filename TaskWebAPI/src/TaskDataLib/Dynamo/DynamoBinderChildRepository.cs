﻿using HoloLC.TaskDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskCoreLib.Models;
using NLog;
using AWSCore.DynamoBase.Tables.Core;
using AWSCore.DynamoBase.Repositories.Base;
using Amazon.DynamoDBv2.DocumentModel;
using HoloLC.TaskDataLib.Dynamo.Tables.Converters;

namespace HoloLC.TaskDataLib.Dynamo
{
    public class DynamoBinderChildRepository : BaseDynamoRepository, IBinderChildRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public DynamoBinderChildRepository(IDynamoTable dynamoTable)
            : base(dynamoTable)
        {
        }

        // cf. https://blogs.aws.amazon.com/net/post/Tx1KWKHIN0894B/DynamoDB-Document-Model-Manual-Pagination
        public async Task<IEnumerable<BinderChild>> FindTaskBoardsAsync(ulong binderId)
        {
            try {
                // TBD: No pagination at this point....
                //      --> This should be fixed ASAP.
                var search = DynamoTable.Table?.Query(new QueryOperationConfig {
                    Filter = new QueryFilter("BinderId", QueryOperator.Equal, binderId),
                    Limit = 1000,    // arbitrary, a large number to avoid pagination. TBD: Does it work??? What's the maximum value of Limit?
                });
                IList<Document> items = null;
                if (search != null) {
                    items = await search.GetNextSetAsync();
                }
                // var token = search.PaginationToken;
                if (items != null) {
                    var binderChildren = new List<BinderChild>();
                    if (items.Any()) {
                        foreach (var d in items) {
                            var box = BinderChildConverter.ConvertFromDocument(d);
                            if (box != null) {
                                binderChildren.Add(box);
                            } else {
                                // ???
                                Logger.Warn($"Failed to convert document to BinderChild. document = {d}");
                            }
                        }
                    }
                    Logger.Debug($"binderChildren feteched. Count = {binderChildren.Count}");
                    return binderChildren;
                }
            } catch (Exception ex) {
                Logger.Warn($"FindTaskBoardsAsync() (binder = {binderId}) failed. {ex.Message}");
            }
            // ????
            Logger.Warn($"search.GetNextSetAsync() failed.");
            return null;
        }


        public async Task<bool> AddBinderChildAsync(BinderChild binderChild)
        {
            Logger.Trace($"AddBinderChildAsync() binderChild = {binderChild}.");

            try {
                var document = BinderChildConverter.ConvertToDocument(binderChild);
                if (document != null) {
                    await DynamoTable.Table?.UpdateItemAsync(document);
                    // TBD: How to check if UpdateItemAsync() was successful???
                    if (binderChild != null) {
                        Logger.Debug($"BinderChild saved: binderId = {binderChild.BinderId}. boardId = {binderChild.BoardId}.");
                        return true;
                    } else {
                        Logger.Info($"Table.UpdateItemAsync() failed.");
                        return false;
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"AddBinderChildAsync() (binderChild = {binderChild}) failed. {ex.Message}");
            }
            Logger.Info($"AddBinderChildAsync() failed.");
            return false;
        }

        public async Task<bool> ReplaceBinderChildAsync(BinderChild binderChild, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceBinderChildAsync() binderChild = {binderChild}; createIfNotExist = {createIfNotExist}.");

            var saved = false;
            try {
                var binderId = binderChild.BinderId;
                var boardId = binderChild.BoardId;
                var existingDocument = await DynamoTable.Table?.GetItemAsync(binderId, boardId);
                if (existingDocument != null) {
                    var newDocument = BinderChildConverter.ConvertToDocument(binderChild);
                    if (newDocument != null) {
                        // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        await DynamoTable.Table?.UpdateItemAsync(newDocument);
                    }
                    // tbd: How do you know if UpdateItemAsync() was successful???
                    if (newDocument != null) {
                        saved = true;
                        Logger.Debug($"BinderChild replaced: boardId = {boardId}. binderId = {binderId}.");
                    } else {
                        Logger.Warn($"Failed to replace BinderChild: boardId = {boardId}. binderId = {binderId}.");
                    }
                } else {
                    if (createIfNotExist) {
                        var newDocument = BinderChildConverter.ConvertToDocument(binderChild);
                        if (newDocument != null) {
                            // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                            await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        }
                        // tbd: How do you know if UpdateItemAsync() was successful???
                        if (newDocument != null) {
                            saved = true;
                            Logger.Debug($"New pickupbox added: owner = {boardId}. id = {binderId}.");
                        } else {
                            Logger.Warn($"Failed to add new BinderChild: owner = {boardId}. id = {binderId}.");
                        }
                    } else {
                        Logger.Info($"BinderChild not found: owner = {boardId}. id = {binderId}.");
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"ReplaceBinderChildAsync() (binderChild = {binderChild}) failed. {ex.Message}");
            }
            return saved;
        }


        public async Task<BinderChild> DeleteBinderChildAsync(ulong binderId, ulong boardId)
        {
            Logger.Trace($"DeleteBinderChildAsync() binderId = {binderId}; boardId = {boardId}.");

            try {
                var existingDocument = await DynamoTable.Table?.GetItemAsync(binderId, boardId);
                if (existingDocument != null) {
                    var binderChild = BinderChildConverter.ConvertFromDocument(existingDocument);
                    await DynamoTable.Table?.DeleteItemAsync(existingDocument);
                    // tbd: How do you know if DeleteItemAsync() was successful???
                    if (binderChild != null) {
                        Logger.Debug($"BinderChild deleted: boardId = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                        return binderChild;
                    } else {
                        Logger.Warn($"Failed to delete pickupbox: binderId = {binderId}; boardId = {boardId}.");
                        return null;
                    }
                } else {
                    Logger.Info($"BinderChild not found for binderId = {binderId}; boardId = {boardId}.");
                    return null;
                }
            } catch (Exception ex) {
                Logger.Warn($"DeleteBinderChildAsync() (binderId = {binderId}; boardId = {boardId}) failed. {ex.Message}");
                return null;
            }
        }

        public async Task<int> RemoveByBinderAsync(ulong binderId)
        {
            var count = 0;
            var list = await FindTaskBoardsAsync(binderId);
            if (list != null) {
                foreach (var box in list) {
                    var d = BinderChildConverter.ConvertToDocument(box);
                    if (d != null) {
                        await DynamoTable.Table?.DeleteItemAsync(d);
                        count++;
                    }
                }
            }
            return count;
        }

    }
}
