﻿using HoloLC.TaskDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.TaskCoreLib.Models;
using NLog;
using AWSCore.DynamoBase.Tables.Core;
using AWSCore.DynamoBase.Repositories.Base;
using HoloLC.TaskDataLib.Dynamo.Tables.Converters;
using Amazon.DynamoDBv2.DocumentModel;

namespace HoloLC.TaskDataLib.Dynamo
{
    public class DynamoTaskBinderRepository : BaseDynamoRepository, ITaskBinderRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public DynamoTaskBinderRepository(IDynamoTable dynamoTable)
            : base(dynamoTable)
        {
        }


        public async Task<TaskBinder> GetItemAsync(ulong id, ulong owner = 0)
        {
            // tbd:
            TaskBinder taskBinder = null;
            try {
                var document = await DynamoTable.Table?.GetItemAsync(id);
                taskBinder = TaskBinderConverter.ConvertFromDocument(document);
                Logger.Debug($"id = {id}. taskBinder found: {taskBinder}.");
            } catch (Exception ex) {
                Logger.Warn($"GetItemAsync() DynamoTable.Table?.GetItemAsync({id}) failed. {ex.Message}");
            }
            return taskBinder;
        }

        // cf. https://blogs.aws.amazon.com/net/post/Tx1KWKHIN0894B/DynamoDB-Document-Model-Manual-Pagination
        public async Task<IEnumerable<TaskBinder>> FindAllByOwnerAsync(ulong owner)
        {
            try {
                // TBD: No pagination at this point....
                //      --> This should be fixed ASAP.
                var search = DynamoTable.Table?.Query(new QueryOperationConfig {
                    Filter = new QueryFilter("Owner", QueryOperator.Equal, owner),
                    Limit = 1000,    // arbitrary, a large number to avoid pagination. TBD: Does it work??? What's the maximum value of Limit?
                });
                IList<Document> items = null;
                if (search != null) {
                    items = await search.GetNextSetAsync();
                }
                // var token = search.PaginationToken;
                if (items != null) {
                    var taskBinders = new List<TaskBinder>();
                    if (items.Any()) {
                        foreach (var d in items) {
                            var box = TaskBinderConverter.ConvertFromDocument(d);
                            if (box != null) {
                                taskBinders.Add(box);
                            } else {
                                // ???
                                Logger.Warn($"Failed to convert document to TaskBinder. document = {d}");
                            }
                        }
                    }
                    Logger.Debug($"taskBinders feteched. Count = {taskBinders.Count}");
                    return taskBinders;
                }
            } catch (Exception ex) {
                Logger.Warn($"FindAllAsync() (owner = {owner}) failed. {ex.Message}");
            }
            // ????
            Logger.Warn($"search.GetNextSetAsync() failed.");
            return null;
        }


        public async Task<bool> AddItemAsync(TaskBinder taskBinder)
        {
            Logger.Trace($"AddItemAsync() taskBinder = {taskBinder}.");

            try {
                var document = TaskBinderConverter.ConvertToDocument(taskBinder);
                if (document != null) {
                    await DynamoTable.Table?.UpdateItemAsync(document);
                    // TBD: How to check if UpdateItemAsync() was successful???
                    if (taskBinder != null) {
                        Logger.Debug($"TaskBinder saved: id = {taskBinder.Id}. owner = {taskBinder.Owner}.");
                        return true;
                    } else {
                        Logger.Info($"Table.UpdateItemAsync() failed.");
                        return false;
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"AddItemAsync() (taskBinder = {taskBinder}) failed. {ex.Message}");
            }
            Logger.Info($"AddItemAsync() failed.");
            return false;
        }

        public async Task<bool> ReplaceItemAsync(TaskBinder taskBinder, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItemAsync() taskBinder = {taskBinder}; createIfNotExist = {createIfNotExist}.");

            var saved = false;
            try {
                var id = taskBinder.Id;
                var owner = taskBinder.Owner;
                var existingDocument = await DynamoTable.Table?.GetItemAsync(id, owner);
                if (existingDocument != null) {
                    var newDocument = TaskBinderConverter.ConvertToDocument(taskBinder);
                    if (newDocument != null) {
                        // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        await DynamoTable.Table?.UpdateItemAsync(newDocument);
                    }
                    // tbd: How do you know if UpdateItemAsync() was successful???
                    if (newDocument != null) {
                        saved = true;
                        Logger.Debug($"TaskBinder replaced: owner = {owner}. id = {id}.");
                    } else {
                        Logger.Warn($"Failed to replace TaskBinder: owner = {owner}. id = {id}.");
                    }
                } else {
                    if (createIfNotExist) {
                        var newDocument = TaskBinderConverter.ConvertToDocument(taskBinder);
                        if (newDocument != null) {
                            // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                            await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        }
                        // tbd: How do you know if UpdateItemAsync() was successful???
                        if (newDocument != null) {
                            saved = true;
                            Logger.Debug($"New pickupbox added: owner = {owner}. id = {id}.");
                        } else {
                            Logger.Warn($"Failed to add new TaskBinder: owner = {owner}. id = {id}.");
                        }
                    } else {
                        Logger.Info($"TaskBinder not found: owner = {owner}. id = {id}.");
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"ReplaceItemAsync() (taskBinder = {taskBinder}) failed. {ex.Message}");
            }
            return saved;
        }


        public async Task<TaskBinder> DeleteItemAsync(ulong id, ulong owner = 0)
        {
            Logger.Trace($"DeleteItemAsync() id = {id}.");

            try {
                var existingDocument = await DynamoTable.Table?.GetItemAsync(id);
                if (existingDocument != null) {
                    var taskBinder = TaskBinderConverter.ConvertFromDocument(existingDocument);
                    await DynamoTable.Table?.DeleteItemAsync(existingDocument);
                    // tbd: How do you know if DeleteItemAsync() was successful???
                    if (taskBinder != null) {
                        Logger.Debug($"TaskBinder deleted: owner = {taskBinder.Owner}. id = {taskBinder.Id}.");
                        return taskBinder;
                    } else {
                        Logger.Warn($"Failed to delete pickupbox: id = {id}.");
                        return null;
                    }
                } else {
                    Logger.Info($"TaskBinder not found for id = {id}.");
                    return null;
                }
            } catch (Exception ex) {
                Logger.Warn($"DeleteItemAsync() (id = {id}) failed. {ex.Message}");
                return null;
            }
        }

        public async Task<int> RemoveAllByOwnerAsync(ulong owner)
        {
            var count = 0;
            var list = await FindAllByOwnerAsync(owner);
            if (list != null) {
                foreach (var box in list) {
                    var d = TaskBinderConverter.ConvertToDocument(box);
                    if (d != null) {
                        await DynamoTable.Table?.DeleteItemAsync(d);
                        count++;
                    }
                }
            }
            return count;
        }

    }
}
