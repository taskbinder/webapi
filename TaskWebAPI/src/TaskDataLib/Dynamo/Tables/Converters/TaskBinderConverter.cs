﻿using Amazon.DynamoDBv2.DocumentModel;
using HoloLC.TaskCoreLib.Core;
using HoloLC.TaskCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskDataLib.Dynamo.Tables.Converters
{
    public static class TaskBinderConverter
    {
        public static Document ConvertToDocument(TaskBinder taskBinder)
        {
            if (taskBinder != null) {
                var document = new Document();
                document.Add(nameof(taskBinder.Id), taskBinder.Id);
                document.Add(nameof(taskBinder.Owner), taskBinder.Owner);
                if (taskBinder.Title != null) document.Add(nameof(taskBinder.Title), taskBinder.Title);
                if (taskBinder.Description != null) document.Add(nameof(taskBinder.Description), taskBinder.Description);
                if (taskBinder.AccessLevel.ToName() != null) document.Add(nameof(taskBinder.AccessLevel), taskBinder.AccessLevel.ToName());
                document.Add(nameof(taskBinder.HomeBoard), taskBinder.HomeBoard);
                document.Add(nameof(taskBinder.Width), taskBinder.Width);
                document.Add(nameof(taskBinder.Height), taskBinder.Height);
                document.Add(nameof(taskBinder.Created), taskBinder.Created);
                document.Add(nameof(taskBinder.Updated), taskBinder.Updated);
                document.Add(nameof(taskBinder.Deleted), taskBinder.Deleted);
                return document;
            } else {
                return null;
            }
        }
        public static TaskBinder ConvertFromDocument(Document document)
        {
            if (document != null) {
                var taskBinder = new TaskBinder();
                taskBinder.Id = document[nameof(taskBinder.Id)].AsULong();
                taskBinder.Owner = document.ContainsKey(nameof(taskBinder.Owner)) ? document[nameof(taskBinder.Owner)].AsULong() : 0UL;
                taskBinder.Title = document.ContainsKey(nameof(taskBinder.Title)) ? document[nameof(taskBinder.Title)].AsString() : "";
                taskBinder.Description = document.ContainsKey(nameof(taskBinder.Description)) ? document[nameof(taskBinder.Description)].AsString() : "";
                taskBinder.AccessLevel = document.ContainsKey(nameof(taskBinder.AccessLevel)) ? document[nameof(taskBinder.AccessLevel)].AsString().ToAccessLevel() : AccessLevel.Unknown;
                taskBinder.HomeBoard = document.ContainsKey(nameof(taskBinder.HomeBoard)) ? document[nameof(taskBinder.HomeBoard)].AsULong() : 0UL;
                taskBinder.Width = document.ContainsKey(nameof(taskBinder.Width)) ? document[nameof(taskBinder.Width)].AsUInt() : 0U;
                taskBinder.Height = document.ContainsKey(nameof(taskBinder.Height)) ? document[nameof(taskBinder.Height)].AsUInt() : 0U;
                taskBinder.Created = document.ContainsKey(nameof(taskBinder.Created)) ? document[nameof(taskBinder.Created)].AsLong() : 0L;
                taskBinder.Updated = document.ContainsKey(nameof(taskBinder.Updated)) ? document[nameof(taskBinder.Updated)].AsLong() : 0L;
                taskBinder.Deleted = document.ContainsKey(nameof(taskBinder.Deleted)) ? document[nameof(taskBinder.Deleted)].AsLong() : 0L;
                return taskBinder;
            } else {
                return null;
            }
        }

    }
}
