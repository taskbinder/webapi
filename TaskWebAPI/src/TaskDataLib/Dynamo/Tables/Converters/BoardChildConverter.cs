﻿using Amazon.DynamoDBv2.DocumentModel;
using HoloLC.TaskCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.TaskDataLib.Dynamo.Tables.Converters
{
    public static class BoardChildConverter
    {
        public static Document ConvertToDocument(BoardChild boardChild)
        {
            if (boardChild != null) {
                var document = new Document();
                document.Add(nameof(boardChild.ParentId), boardChild.ParentId);
                document.Add(nameof(boardChild.ChildId), boardChild.ChildId);
                if (boardChild.ChildTitle != null) document.Add(nameof(boardChild.ChildTitle), boardChild.ChildTitle);
                document.Add(nameof(boardChild.ChildZOrder), boardChild.ChildZOrder);
                document.Add(nameof(boardChild.Created), boardChild.Created);
                document.Add(nameof(boardChild.Updated), boardChild.Updated);
                document.Add(nameof(boardChild.Deleted), boardChild.Deleted);
                return document;
            } else {
                return null;
            }
        }
        public static BoardChild ConvertFromDocument(Document document)
        {
            if (document != null) {
                var boardChild = new BoardChild();
                boardChild.ParentId = document[nameof(boardChild.ParentId)].AsULong();
                boardChild.ChildId = document[nameof(boardChild.ChildId)].AsULong();
                boardChild.ChildTitle = document.ContainsKey(nameof(boardChild.ChildTitle)) ? document[nameof(boardChild.ChildTitle)].AsString() : "";
                boardChild.ChildZOrder = document.ContainsKey(nameof(boardChild.ChildZOrder)) ? document[nameof(boardChild.ChildZOrder)].AsInt() : 0;
                boardChild.Created = document.ContainsKey(nameof(boardChild.Created)) ? document[nameof(boardChild.Created)].AsLong() : 0L;
                boardChild.Updated = document.ContainsKey(nameof(boardChild.Updated)) ? document[nameof(boardChild.Updated)].AsLong() : 0L;
                boardChild.Deleted = document.ContainsKey(nameof(boardChild.Deleted)) ? document[nameof(boardChild.Deleted)].AsLong() : 0L;
                return boardChild;
            } else {
                return null;
            }
        }

    }
}
